with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx

package body Kernels with
  SPARK_Mode
is

   function Cuda_Index
     (Block_Dim, Block_Idx, Thread_Idx : unsigned) return Natural with
     SPARK_Mode => Off
   is
   begin

      return Natural (Block_Dim * Block_Idx + Thread_Idx);

   end Cuda_Index;

   procedure VectorCount
     (A : not null Vector_Device_Constant_Access;
      B : not null Counter_Array_Device_Access)
   is

      -------- Mirror wrapper's precondition semantics with assumptions --------
      X : Natural := Cuda_Index (Block_Dim.X, Block_Idx.X, Thread_Idx.X);

      pragma Assume (A'Last <= Integer'Last - 31);

      Max_X : Integer := ((Integer (B'Last) + 31) / 32) * 32;
      pragma Assume (X in 0 .. Max_X);
      pragma Assume (A'First = 0 and A'Last <= Natural'Last - 1);
      --------------------------------------------------------------------------

      Idx : Int_1000;
      Sum : Natural := 0;

   begin

      if X <= Integer (B'Last) then
         Idx := Int_1000 (X);

         for I in A'Range loop
            pragma Loop_Invariant (Sum <= Sum'Loop_Entry + I);
            if A (I) = Idx then
               Sum := Sum + 1;
            end if;
         end loop;

         B (Idx) := Sum;

      end if;

   end VectorCount;

end Kernels;
