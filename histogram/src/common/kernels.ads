with Interfaces.C;        use Interfaces.C;
with CUDA.Vector_Types;   use CUDA.Vector_Types;
with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernels with
  SPARK_Mode
is

   type Pos3 is record
      X : Positive;
      Y : Positive;
      Z : Positive;
   end record;

   type Int_1000 is new Integer range 0 .. 1_000;

   type Vector is array (Natural range <>) of Int_1000;
   type Counter_Array is array (Int_1000'Range) of Natural;

   type Vector_Device_Access is access Vector;
   --with Designated_Storage_Model => CUDA.Storage_Models.Model;

   type Vector_Device_Constant_Access is access constant Vector;

   type Counter_Array_Device_Access is access Counter_Array;
   --with Designated_Storage_Model => CUDA.Storage_Models.Model;

   function Cuda_Index
     (Block_Dim, Block_Idx, Thread_Idx : unsigned) return Natural;

   procedure VectorCount
     (A : not null Vector_Device_Constant_Access;
      B : not null Counter_Array_Device_Access) with
     Pre => A /= null and then B /= null, Cuda_Global;

end Kernels;
