with Ada.Text_IO; use Ada.Text_IO;

with Kernels;          use Kernels;
with Kernel_Wrappers;  use Kernel_Wrappers;
with Vector_Utilities; use Vector_Utilities;

procedure Main with
  SPARK_Mode
is

   Vector_Size : constant Positive := 2**20;

   A         : Vector (0 .. Vector_Size - 1);
   B, B_Host : Counter_Array;

   Threads_Per_Block : Pos3 := (2**8, 1, 1);
   Blocks_Per_Grid   : Pos3 :=
     ((Counter_Array'Length + Threads_Per_Block.X - 1) / Threads_Per_Block.X,
      1, 1);

begin

   -- Initialize vectors
   Generate_Vector (A);

   -- Perform a calculation using A and B, resulting in C on the CPU
   Vector_Count_Host (A, B_Host);

   -- Perform a calculation using A and B, resulting in C on the GPU
   VectorCountWrapper (Threads_Per_Block, Blocks_Per_Grid, A, B, Vector_Size);

   for I in B'Range loop
      if B (I) /= B_Host (I) then
         Put_Line ("Test FAILED");
         return;
      end if;
   end loop;

   Put_Line ("Test SUCCEDDED");

end Main;
