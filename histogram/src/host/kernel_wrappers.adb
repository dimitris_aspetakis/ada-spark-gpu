with CUDA.Runtime_Api; use CUDA.Runtime_Api;
with Kernels;          use Kernels;
with Ada.Unchecked_Deallocation;
with Ada.Unchecked_Conversion;
with Ada.Text_IO; use Ada.Text_IO;

package body Kernel_Wrappers with
  SPARK_Mode => Off -- TODO: Maybe turn it on when Cuda_Execute is supported.
is

   procedure VectorCountWrapper
     (Threads_Per_Block :     Pos3; Blocks_Per_Grid : Pos3; A : Vector;
      B                 : out Counter_Array; Vector_Size : Positive)
   is

      procedure Free is new Ada.Unchecked_Deallocation
        (Vector, Vector_Device_Access);

      procedure Free is new Ada.Unchecked_Deallocation
        (Counter_Array, Counter_Array_Device_Access);

      D_A : Vector_Device_Access        := new Vector (A'Range);
      D_B : Counter_Array_Device_Access := new Counter_Array;

   begin

      D_A.all := A;

      Put_Line (Pos3'Image (Threads_Per_Block) & "   " & Pos3'Image (Blocks_Per_Grid));
      pragma Cuda_Execute
        (VectorCount (Vector_Device_Constant_Access (D_A), D_B),
         (Blocks_Per_Grid.X, Blocks_Per_Grid.Y, Blocks_Per_Grid.Z),
         (Threads_Per_Block.X, Threads_Per_Block.Y, Threads_Per_Block.Z));

      B := D_B.all;

      Free (D_A);
      Free (D_B);

   end VectorCountWrapper;

end Kernel_Wrappers;
