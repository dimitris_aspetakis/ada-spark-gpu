with Kernels; use Kernels;

package Vector_Utilities with
  SPARK_Mode => On
is

   procedure Vector_Count_Host (A : Vector; B : out Counter_Array);

   procedure Generate_Vector (V : out Vector);

   procedure Print_Counter_Array (V : Counter_Array);

end Vector_Utilities;
