with Text_IO;
with Ada.Numerics.Discrete_Random;

package body Vector_Utilities is

   procedure Vector_Count_Host (A : Vector; B : out Counter_Array) is
   begin

      for Idx in B'Range loop
         B (Idx) := 0;
         for I in A'Range loop
            if A (I) = Idx then
               B (Idx) := B (Idx) + 1;
            end if;
         end loop;
      end loop;

   end Vector_Count_Host;

   procedure Generate_Vector (V : out Vector) is

      package Rand_Int_1000 is new Ada.Numerics.Discrete_Random (Int_1000);
      G : Rand_Int_1000.Generator;

   begin

      -- Initialize the random number generator
      Rand_Int_1000.Reset (G);

      V := (others => Rand_Int_1000.Random (G));

   end Generate_Vector;

   procedure Print_Counter_Array (V : Counter_Array) is
   begin

      for i in V'Range loop
         Text_IO.Put (Natural'Image (V (i)));
         Text_IO.Put (" ");
      end loop;
      Text_IO.New_Line;

   end Print_Counter_Array;

end Vector_Utilities;
