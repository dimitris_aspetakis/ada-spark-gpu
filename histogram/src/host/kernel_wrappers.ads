with Interfaces.C; use Interfaces.C;
with Kernels;      use Kernels;

pragma Overflow_Mode (General => Eliminated, Assertions => Eliminated);

package Kernel_Wrappers with
  SPARK_Mode => On
is

   procedure VectorCountWrapper
     (Threads_Per_Block :     Pos3; Blocks_Per_Grid : Pos3; A : Vector;
      B                 : out Counter_Array; Vector_Size : Positive) with
     Pre =>
      Threads_Per_Block.X * Blocks_Per_Grid.X in Positive'Range
      and then (A'First = 0 and B'First = 0 and A'Last = Vector_Size - 1);

end Kernel_Wrappers;
