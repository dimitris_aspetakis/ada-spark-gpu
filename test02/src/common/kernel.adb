with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx

package body Kernel with
  SPARK_Mode
is

   procedure VectorAdd
     (A : Vector_Device_Access; B : Vector_Device_Access;
      C : Fat_Vector_Device_Access)
   is

      ----- Passing information from code on the Host - unsafe assumptions -----
      X : Integer := Integer (Block_Dim.X * Block_Idx.X + Thread_Idx.X);

      Vector_Size : constant Positive := 262_144;

      pragma Assume (A'First = B'First and A'First = C'First);
      pragma Assume (A'Last = B'Last and A'Last = C'Last);
      pragma Assume (A'Last <= Integer'Last - 31);

      Max_X : Integer := ((A'Last + 31) / 32) * 32;
      pragma Assume (X in A'First .. Max_X);
      --------------------------------------------------------------------------

   begin
      if X <= A'Last then
         C (X) := Int_20 (A (X) + B (X) + 1);
         C (X) := Int_20 (A (X) + B (X) - 1);
      end if;

   end VectorAdd;

end Kernel;
