with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernel with
  SPARK_Mode
is

   type Int_10 is new Integer range -10 .. 10;
   type Int_20 is new Integer range -20 .. 20;

   type Vector is array (Natural range <>) of Int_10;
   type Fat_Vector is array (Natural range <>) of Int_20;

   type Vector_Device_Access is access Vector;
   --with Designated_Storage_Model => CUDA.Storage_Models.Model;

   type Fat_Vector_Device_Access is access Fat_Vector;
   --with Designated_Storage_Model => CUDA.Storage_Models.Model;

   procedure VectorAdd
     (A : Vector_Device_Access; B : Vector_Device_Access;
      C : Fat_Vector_Device_Access) with
     Pre =>
      A /= null and then B /= null and then C /= null
      and then A'Length = B'Length and then A'Length = C'Length
      and then A'Length in Natural'Range,
     Cuda_Global;

end Kernel;
