with System;
with Interfaces.C; use Interfaces.C;

with Ada.Real_Time; use Ada.Real_Time;
with Ada.Text_IO;   use Ada.Text_IO;

with CUDA.Driver_Types;   use CUDA.Driver_Types;
with CUDA.Runtime_Api;    use CUDA.Runtime_Api;
with CUDA.Vector_Types;   use CUDA.Vector_Types;
with CUDA.Stddef;
with CUDA.Storage_Models; use CUDA.Storage_Models;

with Kernel;           use Kernel;
with Vector_Utilities; use Vector_Utilities;

with Ada.Unchecked_Deallocation;
with Ada.Unchecked_Conversion;

procedure Main with
  SPARK_Mode
is

   type Vector_Host_Access is access Vector;
   type Fat_Vector_Host_Access is access Fat_Vector;

   procedure Free is new Ada.Unchecked_Deallocation
     (Vector, Vector_Host_Access);

   procedure Free is new Ada.Unchecked_Deallocation
     (Vector, Vector_Device_Access);

   procedure Free is new Ada.Unchecked_Deallocation
     (Fat_Vector, Fat_Vector_Host_Access);

   procedure Free is new Ada.Unchecked_Deallocation
     (Fat_Vector, Fat_Vector_Device_Access);

   Vector_Size : Integer := 262_144;

   H_A, H_B : Vector_Host_Access       := new Vector (0 .. Vector_Size - 1);
   D_A, D_B : Vector_Device_Access     := new Vector (0 .. Vector_Size - 1);
   H_C      : Fat_Vector_Host_Access := new Fat_Vector (0 .. Vector_Size - 1);
   D_C : Fat_Vector_Device_Access := new Fat_Vector (0 .. Vector_Size - 1);

   Threads_Per_Block : Dim3 := (256, 1, 1);
   Blocks_Per_Grid   : Dim3 :=
     ((unsigned (Vector_Size) + Threads_Per_Block.X - 1) / Threads_Per_Block.X,
      1, 1);

   Start_Time   : Time;
   Elapsed_Time : Time_Span;
   Err          : Error_T;

begin

   -- Initialize vectors
   Generate_Vector (H_A.all);
   Generate_Vector (H_B.all);

   -- Initialize device vectors
   D_A.all := H_A.all;
   D_B.all := H_B.all;

   Put_Line
     ("CUDA kernel launch with " & Blocks_Per_Grid'Img & " blocks of " &
      Threads_Per_Block'Img & "  threads");

   -- Perform a calculation using A and B, resulting in C (while measuring
   -- elapsed time too)
   -- TODO: check if timing results are correct
   Start_Time := Clock;
   pragma Cuda_Execute
     (VectorAdd (D_A, D_B, D_C), Blocks_Per_Grid, Threads_Per_Block);
   Elapsed_Time := Clock - Start_Time;

   Err := Get_Last_Error;

   Put_Line ("Copy output data from the CUDA device to the host memory");

   H_C.all := D_C.all;

   -- Print the duration of the benchmark's kernel
   Put_Line ("Elapsed kernel time: " & Time_Span'Image (Elapsed_Time));

   -- Free matrices
   Free (D_A);
   Free (D_B);
   Free (D_C);

   Free (H_A);
   Free (H_B);
   Free (H_C);

   Put_Line ("Test COMPLETED");

end Main;
