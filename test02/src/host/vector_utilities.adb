with Text_IO;
with Ada.Numerics.Discrete_Random;

package body Vector_Utilities is

   procedure Generate_Vector (M : out Vector) is

      package Rand_Int_10 is new Ada.Numerics.Discrete_Random (Int_10);
      G : Rand_Int_10.Generator;

   begin

      -- Initialize the random number generator
      Rand_Int_10.Reset (G);

      M := (others => Rand_Int_10.Random (G));

   end Generate_Vector;

   procedure Print_Vector (M : Vector) is
   begin

      for i in M'Range loop
         Text_IO.Put (Int_10'Image (M (i)));
         Text_IO.Put (" ");
      end loop;
      Text_IO.New_Line;

   end Print_Vector;

end Vector_Utilities;
