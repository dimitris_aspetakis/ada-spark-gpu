pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package udevice_types_h is

   type cudaRoundMode is 
     (cudaRoundNearest,
      cudaRoundZero,
      cudaRoundPosInf,
      cudaRoundMinInf);
   pragma Convention (C, cudaRoundMode);  -- /usr/local/cuda-11.5/include//device_types.h:68

end udevice_types_h;
