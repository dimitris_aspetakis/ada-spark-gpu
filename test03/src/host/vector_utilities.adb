with Text_IO;
with Ada.Numerics.Discrete_Random;

package body Vector_Utilities is

   procedure Vector_Div (A, B : Vector; C : out Vector) is
   begin

      for I in A'Range loop
         C (I) := A (I) / B (I);
      end loop;

   end Vector_Div;

   procedure Generate_Vector (V : out Vector) is

      package Rand_Safe_Div_Int is new Ada.Numerics.Discrete_Random
        (Safe_Div_Int);
      G : Rand_Safe_Div_Int.Generator;

   begin

      -- Initialize the random number generator
      Rand_Safe_Div_Int.Reset (G);

      V := (others => Rand_Safe_Div_Int.Random (G));

   end Generate_Vector;

   procedure Print_Vector (V : Vector) is
   begin

      for i in V'Range loop
         Text_IO.Put (Safe_Div_Int'Image (V (i)));
         Text_IO.Put (" ");
      end loop;
      Text_IO.New_Line;

   end Print_Vector;

end Vector_Utilities;
