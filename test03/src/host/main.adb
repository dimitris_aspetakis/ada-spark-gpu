with Ada.Text_IO; use Ada.Text_IO;

with Kernels;          use Kernels;
with Kernel_Wrappers;  use Kernel_Wrappers;
with Vector_Utilities; use Vector_Utilities;

procedure Main with
  SPARK_Mode
is

   Vector_Size : constant Positive := 2**18;

   A, B, C, C_Host : Vector (0 .. Vector_Size - 1);

   Threads_Per_Block : Pos3 := (2**8, 1, 1);
   Blocks_Per_Grid   : Pos3 :=
     ((Vector_Size + Threads_Per_Block.X - 1) / Threads_Per_Block.X, 1, 1);

begin

   -- Initialize vectors
   Generate_Vector (A);
   Generate_Vector (B);

   Vector_Div (A, B, C_Host);

   -- Perform a calculation using A and B, resulting in C
   VectorDivWrapper (Threads_Per_Block, Blocks_Per_Grid, A, B, C);

   for I in C'Range loop
      if C (I) /= C_Host (I) then
         Put_Line ("Test FAILED");
         return;
      end if;
   end loop;

   Put_Line ("Test SUCCEEDED");

end Main;
