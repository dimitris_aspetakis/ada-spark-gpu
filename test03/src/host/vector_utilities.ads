with Kernels; use Kernels;

package Vector_Utilities with
  SPARK_Mode => On
is

   procedure Vector_Div (A, B : Vector; C : out Vector) with
     Pre =>
      (A'First = B'First and then B'First = C'First) and
      (A'Last = B'Last and then B'Last = C'Last);

   procedure Generate_Vector (V : out Vector);

   procedure Print_Vector (V : Vector);

end Vector_Utilities;
