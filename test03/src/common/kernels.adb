with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx

package body Kernels with
  SPARK_Mode
is

   function Cuda_Index
     (Block_Dim, Block_Idx, Thread_Idx : unsigned) return Natural with
     SPARK_Mode => Off
   is
   begin

      return Natural (Block_Dim * Block_Idx + Thread_Idx);

   end Cuda_Index;

   procedure VectorDiv
     (A, B : not null Vector_Device_Constant_Access;
      C    : not null Vector_Device_Access)
   is

      -------- Mirror wrapper's precondition semantics with assumptions --------
      X : Natural := Cuda_Index (Block_Dim.X, Block_Idx.X, Thread_Idx.X);

      pragma Assume (A'First = 0 and B'First = 0 and C'First = 0);
      pragma Assume (A'Last = B'Last and then B'Last = C'Last);
      pragma Assume (A'Last <= Integer'Last - 31);

      Max_X : Integer := ((A'Last + 31) / 32) * 32;
      pragma Assume (X in 0 .. Max_X);
      --------------------------------------------------------------------------

   begin

      if X <= A'Last and then B (X) /= 0 then
         C (X) := A (X) / B (X);
      elsif X <= A'Last then
         case A (X) is
            when 0 =>
               C (X) := 0;
            when 1 .. Safe_Div_Int'Last =>
               C (X) := Safe_Div_Int'Last;
            when Safe_Div_Int'First .. -1 =>
               C (X) := Safe_Div_Int'First;
         end case;
      end if;

   end VectorDiv;

end Kernels;
