with Interfaces.C;        use Interfaces.C;
with CUDA.Vector_Types;   use CUDA.Vector_Types;
with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernels with
  SPARK_Mode
is

   type Pos3 is record
      X : Positive;
      Y : Positive;
      Z : Positive;
   end record;

   type Safe_Div_Int is new Integer range Integer'First + 1 .. Integer'Last;

   type Vector is array (Natural range <>) of Safe_Div_Int;

   type Vector_Device_Access is access Vector;
   --with Designated_Storage_Model => CUDA.Storage_Models.Model;

   type Vector_Device_Constant_Access is access constant Vector;

   function Cuda_Index
     (Block_Dim, Block_Idx, Thread_Idx : unsigned) return Natural;

   procedure VectorDiv
     (A, B : not null Vector_Device_Constant_Access;
      C    : not null Vector_Device_Access) with
     Pre => A /= null and then B /= null and then C /= null, Cuda_Global;

end Kernels;
