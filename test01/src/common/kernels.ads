with Interfaces.C;        use Interfaces.C;
with CUDA.Vector_Types;   use CUDA.Vector_Types;
with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernels with
  SPARK_Mode
is

   type Pos3 is record
      X : Positive;
      Y : Positive;
      Z : Positive;
   end record;

   type Int_10 is new Integer range -10 .. 10;
   type Int_20 is new Integer range -20 .. 20;

   type Vector is array (Natural range <>) of Int_10;
   type Fat_Vector is array (Natural range <>) of Int_20;

   type Vector_Device_Access is access Vector;
   --with Designated_Storage_Model => CUDA.Storage_Models.Model;

   type Vector_Device_Constant_Access is access constant Vector;

   type Fat_Vector_Device_Access is access Fat_Vector;
   --with Designated_Storage_Model => CUDA.Storage_Models.Model;

   function Cuda_Index
     (Block_Dim, Block_Idx, Thread_Idx : unsigned) return Natural;

   procedure VectorAdd
     (A, B : not null Vector_Device_Constant_Access;
      C    : not null Fat_Vector_Device_Access) with
     Pre => A /= null and then B /= null and then C /= null, Cuda_Global;

end Kernels;
