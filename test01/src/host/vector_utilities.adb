with Text_IO;
with Ada.Numerics.Discrete_Random;

package body Vector_Utilities is

   procedure Vector_Add (A, B : Vector; C : out Fat_Vector) is
   begin

      for I in A'Range loop
         C (I) := Int_20 (A (I) + B (I));
      end loop;

   end Vector_Add;

   procedure Generate_Vector (V : out Vector) is

      package Rand_Int_10 is new Ada.Numerics.Discrete_Random (Int_10);
      G : Rand_Int_10.Generator;

   begin

      -- Initialize the random number generator
      Rand_Int_10.Reset (G);

      V := (others => Rand_Int_10.Random (G));

   end Generate_Vector;

   procedure Print_Vector (V : Fat_Vector) is
   begin

      for i in V'Range loop
         Text_IO.Put (Int_20'Image (V (i)));
         Text_IO.Put (" ");
      end loop;
      Text_IO.New_Line;

   end Print_Vector;

end Vector_Utilities;
