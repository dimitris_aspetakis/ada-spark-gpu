with CUDA.Runtime_Api; use CUDA.Runtime_Api;
with Kernels;          use Kernels;
with Ada.Unchecked_Deallocation;
with Ada.Unchecked_Conversion;

package body Kernel_Wrappers with
  SPARK_Mode => Off -- TODO: Maybe turn it on when Cuda_Execute is supported.
is

   procedure VectorAddWrapper
     (Threads_Per_Block :     Pos3; Blocks_Per_Grid : Pos3; A, B : Vector;
      C                 : out Fat_Vector)
   is

      procedure Free is new Ada.Unchecked_Deallocation
        (Vector, Vector_Device_Access);

      procedure Free is new Ada.Unchecked_Deallocation
        (Fat_Vector, Fat_Vector_Device_Access);

      D_A : Vector_Device_Access     := new Vector (A'Range);
      D_B : Vector_Device_Access     := new Vector (B'Range);
      D_C : Fat_Vector_Device_Access := new Fat_Vector (C'Range);

   begin

      D_A.all := A;
      D_B.all := B;

      pragma Cuda_Execute
        (VectorAdd
           (Vector_Device_Constant_Access (D_A),
            Vector_Device_Constant_Access (D_B), D_C),
         (Blocks_Per_Grid.X, Blocks_Per_Grid.Y, Blocks_Per_Grid.Z),
         (Threads_Per_Block.X, Threads_Per_Block.Y, Threads_Per_Block.Z));

      C := D_C.all;

      Free (D_A);
      Free (D_B);
      Free (D_C);

   end VectorAddWrapper;

end Kernel_Wrappers;
