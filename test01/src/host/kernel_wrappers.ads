with Interfaces.C; use Interfaces.C;
with Kernels;      use Kernels;

pragma Overflow_Mode (General => Eliminated, Assertions => Eliminated);

package Kernel_Wrappers with
  SPARK_Mode => On
is

   procedure VectorAddWrapper
     (Threads_Per_Block :     Pos3; Blocks_Per_Grid : Pos3; A, B : Vector;
      C                 : out Fat_Vector) with
     Pre =>
      Threads_Per_Block.X * Blocks_Per_Grid.X in Positive'Range
      and then
      (A'First = 0 and B'First = 0 and C'First = 0 and
       A'Last = (Threads_Per_Block.X * Blocks_Per_Grid.X) - 1 and
       B'Last = (Threads_Per_Block.X * Blocks_Per_Grid.X) - 1 and
       C'Last = (Threads_Per_Block.X * Blocks_Per_Grid.X) - 1);

end Kernel_Wrappers;
