with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx

package body Kernel with
  SPARK_Mode
is

   function Cuda_Index
     (Block_Dim, Block_Idx, Thread_Idx : unsigned) return Natural with
     SPARK_Mode => Off
   is
   begin

      return Natural (Block_Dim * Block_Idx + Thread_Idx);

   end Cuda_Index;

   procedure AvgGrades
     (A : Grade_Vector_Device_Access; B : Grade_Vector_Device_Access;
      C : Grade_Vector_Device_Access)
   is

      ----- Passing information from code on the Host - unsafe assumptions -----
      X : Integer := Cuda_Index (Block_Dim.X, Block_Idx.X, Thread_Idx.X);

      Vector_Size : constant Positive := 262_144;

      pragma Assume (A'First = B'First and A'First = C'First);
      pragma Assume (A'Last = B'Last and A'Last = C'Last);
      pragma Assume (A'Last <= Integer'Last - 31);

      Max_X : Integer := ((A'Last + 31) / 32) * 32;
      pragma Assume (X in A'First .. Max_X);
      --------------------------------------------------------------------------

      type Grade_20 is delta 0.1 digits 3 range 0.0 .. 20.0;
      tmp : Grade_20;

   begin

      if X <= A'Last then
         tmp   := Grade_20 (A (X) + B (X));
         tmp   := tmp / 2;
         C (X) := Grade (tmp);
      end if;

   end AvgGrades;

end Kernel;
