with CUDA.Storage_Models; use CUDA.Storage_Models;
with Interfaces.C;        use Interfaces.C;

package Kernel with
  SPARK_Mode
is

   type Grade is delta 0.1 digits 3 range 0.0 .. 10.0;

   type Grade_Vector is array (Natural range <>) of Grade;

   type Grade_Vector_Device_Access is access Grade_Vector;
   --with Designated_Storage_Model => CUDA.Storage_Models.Model;

   function Cuda_Index
     (Block_Dim, Block_Idx, Thread_Idx : unsigned) return Natural;

   procedure AvgGrades
     (A : Grade_Vector_Device_Access; B : Grade_Vector_Device_Access;
      C : Grade_Vector_Device_Access) with
     Pre =>
      A /= null and then B /= null and then C /= null
      and then A'Length = B'Length and then A'Length = C'Length
      and then A'Length in Natural'Range,
     Cuda_Global;

end Kernel;
