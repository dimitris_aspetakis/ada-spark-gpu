with Kernel; use Kernel;

package Grade_Vector_Utilities is

   procedure Generate_Grade_Vector (M : out Grade_Vector);

   procedure Print_Grade_Vector (M : Grade_Vector);

end Grade_Vector_Utilities;
