with System;
with Interfaces.C; use Interfaces.C;

with Ada.Real_Time; use Ada.Real_Time;
with Ada.Text_IO;   use Ada.Text_IO;

with CUDA.Driver_Types;   use CUDA.Driver_Types;
with CUDA.Runtime_Api;    use CUDA.Runtime_Api;
with CUDA.Vector_Types;   use CUDA.Vector_Types;
with CUDA.Stddef;
with CUDA.Storage_Models; use CUDA.Storage_Models;

with Kernel;                 use Kernel;
with Grade_Vector_Utilities; use Grade_Vector_Utilities;

with Ada.Unchecked_Deallocation;
with Ada.Unchecked_Conversion;

procedure Main with
  SPARK_Mode
is

   type Grade_Vector_Host_Access is access Grade_Vector;

   procedure Free is new Ada.Unchecked_Deallocation
     (Grade_Vector, Grade_Vector_Host_Access);

   procedure Free is new Ada.Unchecked_Deallocation
     (Grade_Vector, Grade_Vector_Device_Access);

   Grade_Vector_Size : Integer := 262_144;

   H_A, H_B, H_C : Grade_Vector_Host_Access   :=
     new Grade_Vector (0 .. Grade_Vector_Size - 1);
   D_A, D_B, D_C : Grade_Vector_Device_Access :=
     new Grade_Vector (0 .. Grade_Vector_Size - 1);

   Threads_Per_Block : Dim3 := (256, 1, 1);
   Blocks_Per_Grid   : Dim3 :=
     ((unsigned (Grade_Vector_Size) + Threads_Per_Block.X - 1) /
      Threads_Per_Block.X,
      1, 1);

   Start_Time   : Time;
   Elapsed_Time : Time_Span;
   Err          : Error_T;

begin

   -- Initialize vectors
   Generate_Grade_Vector (H_A.all);
   Generate_Grade_Vector (H_B.all);

   -- Initialize device vectors
   D_A.all := H_A.all;
   D_B.all := H_B.all;

   Put_Line
     ("CUDA kernel launch with " & Blocks_Per_Grid'Img & " blocks of " &
      Threads_Per_Block'Img & "  threads");

   -- Perform a calculation using A and B, resulting in C (while measuring
   -- elapsed time too)
   Start_Time := Clock;
   pragma Cuda_Execute
     (AvgGrades (D_A, D_B, D_C), Blocks_Per_Grid, Threads_Per_Block);
   Elapsed_Time := Clock - Start_Time;

   Err := Get_Last_Error;

   Put_Line ("Copy output data from the CUDA device to the host memory");

   H_C.all := D_C.all;

   -- Print the duration of the benchmark's kernel
   Put_Line ("Elapsed kernel time: " & Time_Span'Image (Elapsed_Time));

   -- Free matrices
   Free (D_A);
   Free (D_B);
   Free (D_C);

   Free (H_A);
   Free (H_B);
   Free (H_C);

   Put_Line ("Test COMPLETED");

end Main;
