with Text_IO;
with Ada.Numerics.Discrete_Random;

package body Grade_Vector_Utilities is

   procedure Generate_Grade_Vector (M : out Grade_Vector) is

      type Int_20 is new Integer range 0 .. 20;

      package Rand_Int_20 is new Ada.Numerics.Discrete_Random (Int_20);
      G : Rand_Int_20.Generator;

   begin

      -- Initialize the random number generator
      Rand_Int_20.Reset (G);

      M := (others => Grade (Float (Rand_Int_20.Random (G)) / 2.0));

   end Generate_Grade_Vector;

   procedure Print_Grade_Vector (M : Grade_Vector) is
   begin

      for i in M'Range loop
         Text_IO.Put (Grade'Image (M (i)));
         Text_IO.Put (" ");
      end loop;
      Text_IO.New_Line;

   end Print_Grade_Vector;

end Grade_Vector_Utilities;
