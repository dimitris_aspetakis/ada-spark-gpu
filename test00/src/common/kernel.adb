with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx

package body Kernel with
  SPARK_Mode
is

   procedure VectorAdd
     (A : Vector_Device_Access; B : Vector_Device_Access;
      C : Vector_Device_Access)
   is

      X : Integer := Integer (Block_Dim.X * Block_Idx.X + Thread_Idx.X);

   begin

      if X <= A'Last then
         C (X) := A (X) + B (X);
      end if;

   end VectorAdd;

end Kernel;
