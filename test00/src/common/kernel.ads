with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernel with
  SPARK_Mode
is

   type Vector is array (Natural range <>) of Integer;

   type Vector_Device_Access is access Vector with
     Designated_Storage_Model => CUDA.Storage_Models.Model;

   procedure VectorAdd
     (A : Vector_Device_Access; B : Vector_Device_Access;
      C : Vector_Device_Access) with
     Cuda_Global;

end Kernel;
