with Ada.Numerics.Discrete_Random;
with Ada.Text_IO; use Ada.Text_IO;

package body Vector_Utilities is

   procedure Vector_Add (A, B, C : Vector_Host_Access) is
   begin

      for I in A'Range loop
         C (I) := A (I) + B (I);
      end loop;

   end Vector_Add;

   procedure Generate_Vector (M : out Vector) is

      package Rand_Integer is new Ada.Numerics.Discrete_Random (Integer);
      G : Rand_Integer.Generator;

   begin

      -- Initialize the random number generator
      Rand_Integer.Reset (G);

      M := (others => Rand_Integer.Random (G) mod 1_024);

   end Generate_Vector;

   procedure Print_Vector (M : Vector) is
   begin

      for i in M'Range loop
         Put (Integer'Image (M (i)));
         Put (" ");
      end loop;
      New_Line;

   end Print_Vector;

end Vector_Utilities;
