with Kernel; use Kernel;

package Vector_Utilities is

   type Vector_Host_Access is access Vector;

   procedure Vector_Add (A, B, C : Vector_Host_Access);

   procedure Generate_Vector (M : out Vector);

   procedure Print_Vector (M : Vector);

end Vector_Utilities;
