with Interfaces.C; use Interfaces.C;

with Ada.Text_IO; use Ada.Text_IO;

with CUDA.Vector_Types; use CUDA.Vector_Types;

with Kernel;           use Kernel;
with Vector_Utilities; use Vector_Utilities;

with Ada.Unchecked_Deallocation;

procedure Main with
  SPARK_Mode
is

   procedure Free is new Ada.Unchecked_Deallocation
     (Vector, Vector_Host_Access);

   procedure Free is new Ada.Unchecked_Deallocation
     (Vector, Vector_Device_Access);

   Vector_Size : Integer := 1_024;

   C_Model       : Vector_Host_Access   := new Vector (0 .. Vector_Size - 1);
   H_A, H_B, H_C : Vector_Host_Access   := new Vector (0 .. Vector_Size - 1);
   D_A, D_B, D_C : Vector_Device_Access := new Vector (0 .. Vector_Size - 1);

   Threads_Per_Block : Dim3 := (256, 1, 1);
   Blocks_Per_Grid   : Dim3 :=
     ((unsigned (Vector_Size) + Threads_Per_Block.X - 1) / Threads_Per_Block.X,
      1, 1);

begin

   -- Initialize vectors
   Generate_Vector (H_A.all);
   Generate_Vector (H_B.all);

   -- Initialize device vectors
   D_A.all := H_A.all;
   D_B.all := H_B.all;

   Vector_Add (H_A, H_B, C_Model);

   -- Perform a calculation using A and B, resulting in C
   pragma Cuda_Execute
     (VectorAdd (D_A, D_B, D_C), Blocks_Per_Grid, Threads_Per_Block);

   H_C.all := D_C.all;

   for I in C_Model'Range loop
      if H_C (I) /= C_Model (I) then
         Put_Line ("Test FAILED");

         Free (D_A);
         Free (D_B);
         Free (D_C);
         Free (H_A);
         Free (H_B);
         Free (H_C);
         Free (C_Model);

         return;
      end if;
   end loop;

   Free (D_A);
   Free (D_B);
   Free (D_C);
   Free (H_A);
   Free (H_B);
   Free (H_C);
   Free (C_Model);

   Put_Line ("Test SUCCEEDED");

end Main;
