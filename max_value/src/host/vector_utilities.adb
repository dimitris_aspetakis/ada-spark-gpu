with Text_IO;
with Ada.Numerics.Discrete_Random;

package body Vector_Utilities is

   procedure Generate_Vector (M : out Vector) is

      package Rand_Integer is new Ada.Numerics.Discrete_Random (Integer);
      G : Rand_Integer.Generator;

   begin

      -- Initialize the random number generator
      Rand_Integer.Reset (G);

      M := (others => Rand_Integer.Random (G));

   end Generate_Vector;

   procedure Print_Vector (M : Vector) is
   begin

      for i in M'Range loop
         Text_IO.Put (Integer'Image (M (i)));
         Text_IO.Put (" ");
      end loop;
      Text_IO.New_Line;

   end Print_Vector;

end Vector_Utilities;
