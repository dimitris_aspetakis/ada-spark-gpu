with Kernel; use Kernel;

package Vector_Utilities is

   procedure Generate_Vector (M : out Vector);

   procedure Print_Vector (M : Vector);

end Vector_Utilities;
