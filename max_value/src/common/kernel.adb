with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx

package body Kernel with
  SPARK_Mode
is

   procedure VectorMax
     (A : Vector_Device_Access; B : Vector_Device_Access;
      C : Vector_Device_Access)
   is

      ----- Passing information from code on the Host - unsafe assumptions -----
      X : Integer := Integer (Block_Dim.X * Block_Idx.X + Thread_Idx.X);

      Vector_Size : constant Positive := 262_144;

      pragma Assume (A'First = B'First and A'First = C'First);
      pragma Assume (A'Last = B'Last and A'Last = C'Last);
      pragma Assume (A'Last <= Integer'Last - 31);

      Max_X : Integer := ((A'Last + 31) / 32) * 32;
      pragma Assume (X in A'First .. Max_X);
      --------------------------------------------------------------------------

      procedure Max_Apply_All (A, B : Vector; C : in out Vector) with
        Ghost,
        Pre  =>
         (A'First = 0 and B'First = 0 and C'First = 0)
         and then (A'Last = B'Last and A'Last = C'Last),
        Post => (for all I in C'Range => C (I) >= A (I) and C (I) >= B (I))
      is
      begin

         for I in C'Range loop
            C (I) := Integer'Max (A (I), B (I));
            pragma Loop_Invariant
              (for all Idx in C'First .. I =>
                 C (Idx) = Integer'Max (A (Idx), B (Idx)));
         end loop;

      end Max_Apply_All;

      C_Ghost : Vector := C.all with
        Ghost;

   begin

      if X <= A'Last then
         C (X) := Integer'Max (A (X), B (X));
         pragma Assert (C (X) >= A (X) and C (X) >= B (X));
      end if;

      Max_Apply_All (A.all, B.all, C_Ghost);
      pragma Assert
        (for all X in C_Ghost'Range =>
           C_Ghost (X) >= A (X) and C_Ghost (X) >= B (X));

   end VectorMax;

end Kernel;
