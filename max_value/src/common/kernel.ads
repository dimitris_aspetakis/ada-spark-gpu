with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernel with
  SPARK_Mode
is

   type Vector is array (Natural range <>) of Integer;

   type Vector_Device_Access is access Vector;
   --with Designated_Storage_Model => CUDA.Storage_Models.Model;

   procedure VectorMax
     (A : Vector_Device_Access; B : Vector_Device_Access;
      C : Vector_Device_Access) with
     Pre =>
      A /= null and then B /= null and then C /= null
      and then (A'First = 0 and B'First = 0 and C'First = 0)
      and then (A'Last = B'Last and A'Last = C'Last)
      and then A'Length in Natural'Range,
     Cuda_Global;

end Kernel;
